﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LessionTasks.Lession7
{
    public class CreateWriteTextKata
    {
        public static void writeTextWithoutGivenString(string ignore, List<string> inputs)
        {
            string file = "Kata7.txt";

            using (StreamWriter sw = new StreamWriter(file))
            {
                foreach (string input in inputs)
                {
                    if (!input.ToLower().Contains(ignore.ToLower()))
                    {
                        sw.WriteLine(input);
                        sw.WriteLine();
                    }
                }
            }
        }

        public static void writeTextWithoutGivenStringConsole()
        {
            string file = "Kata7.txt";

            Console.WriteLine("Input the ignore string");
            string ignore = Console.ReadLine();

            Console.WriteLine("Input number of lines to write in the file");
            int numberOfLines = Int32.Parse(Console.ReadLine());

            Console.WriteLine($"Input {numberOfLines} strings below");

            List<string> inputs = new List<string>();

            for (int i = 0; i < numberOfLines; i++)
            {
                inputs.Add(Console.ReadLine());
            }

            Console.WriteLine("\nPreparing to write...\n");
            Thread.Sleep(1000);


            using (StreamWriter sw = new StreamWriter(file))
            {
                foreach (string input in inputs)
                {
                    if (!input.ToLower().Contains(ignore.ToLower()))
                    {
                        Console.WriteLine("Writing '" + input + "' to file...\n");
                        Thread.Sleep(1500);
                        sw.WriteLine(input);
                        sw.WriteLine();
                    }
                    else
                    {
                        Console.WriteLine("'" + input + "' contains ignore string, not writing...\n");
                        Thread.Sleep(1500);
                    }
                }
                Console.WriteLine("Complete!");
                Thread.Sleep(1000);
            }
        }

        public static void readFile()
        {
            string file = "Kata7.txt";

            Console.WriteLine("\n\n*******************************************\n\n");
            Console.WriteLine("Content of file: \n\n");

            using (StreamReader sr = new StreamReader(file))
            {
                string line;

                while ((line = sr.ReadLine()) != null)
                {
                    Console.WriteLine(line);
                }
            }
        }
    }
}


