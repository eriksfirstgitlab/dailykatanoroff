﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katas
{
    internal class PhoneNumberWordDecoder
    {
        public static string textToNum(string phoneNumber)
        {
            Dictionary<char, string> decoder = new Dictionary<char, string>()
            {
                { '2',"ABC" },
                { '3',"DEF" },
                { '4',"GHI" },
                { '5',"JKL" },
                { '6',"MNO" },
                { '7',"PQRS" },
                { '8',"TUV" },
                { '9',"WXYZ" },
            };
            foreach (var letter in phoneNumber)
            {
                foreach (var pairValue in decoder)
                {
                    if (pairValue.Value.Contains(letter, StringComparison.OrdinalIgnoreCase))
                    {
                        phoneNumber = phoneNumber.Replace(letter, pairValue.Key);
                    }
                }
            }
            return phoneNumber;
        }

        public static bool isCorrectPhoneNumberFormat(string phoneNumber)
        {
            bool isCorrect = false;
            string correctFormat = ("{XXX-XXX-XXXX}");
            if (String.Format(correctFormat, phoneNumber).CompareTo(correctFormat) > 0)
            {
                isCorrect = true;
            }
            return isCorrect;
        }
    }
}
