﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dailyKata
{
    internal class PigLatinTranslator
    {

        private static char[] vowels = new[] { 'a', 'A', 'e', 'E', 'i', 'I', 'o', 'O', 'u', 'U' };
        public static bool checkVowels(string word)
        {
            /*
            bool isVowel = "aeiouAEIOU".IndexOf(word) >= 0;
            return isVowel;
            */
            int i = 0;

            foreach (var vowel in vowels)
            {
                if (word.ElementAt(0) == vowel)
                {
                    return true;
                }
                else if (word.ElementAt(i) != vowel)
                {
                    if (i < word.Length - 1)
                    {
                        i++;
                        continue;
                    }
                    else
                    {
                        return false;
                    }
                    
                }
                else
                {
                    return true;
                }
                
            }
            return false;
        }
        public static string translateWord(string word)
        {
            string newWord = word;
            if (checkVowels(newWord))
            {
                newWord = newWord + "yay";
                return newWord;
            }
            return newWord;    
        }
    }
}
