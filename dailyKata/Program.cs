﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace dailyKata
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //fizzBuzz.start(); 
            //linqList.linqListOut();
            //UsernameValidation.validate();
            //nineToFive.OverTime(13.25, 15, 30, 1.5);

            /* Test for humanReadableTime
            Console.WriteLine(humanReadableTime.GetReadableTime(0));
            Console.WriteLine(humanReadableTime.GetReadableTime(5));
            Console.WriteLine(humanReadableTime.GetReadableTime(60));
            Console.WriteLine(humanReadableTime.GetReadableTime(86399));
            Console.WriteLine(humanReadableTime.GetReadableTime(359999)); 
            */

            /*
            Console.WriteLine(phoneToWordDecoder.textToNum("123-647-EYES"));
            Console.WriteLine(phoneToWordDecoder.textToNum("(325)444-TEST"));
            Console.WriteLine(phoneToWordDecoder.textToNum("653-TRY-THIS"));
            Console.WriteLine(phoneToWordDecoder.textToNum("435-224-7613"));
            Console.WriteLine(phoneToWordDecoder.textToNum("tIn-AnD-gOlD"));
            Console.WriteLine(phoneToWordDecoder.textToNum("abc-s1d.sdad"));
            */
            /*
            double[] arr = new double[10] { 2, 5, -4, 11, 0, 18, 22, 67, 51, 6 };
            double[] arr2 = new double[5] { 2.22, 20.5, 2.7, -1, 11.2 };
            heapSort heap = new heapSort();
            heap.SortArray(arr, 10);
            heap.SortArray(arr2, 5);
            */

            //selectionSort.SelectionSort(arr2);

            //structure.Square();
            christmasTree.writeAnyTrees(10000);
        }
    }
}