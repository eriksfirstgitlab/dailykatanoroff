﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dailyKata
{
    class RemoveTheWord
    {
        public string[] RemoveLetters(string[] letters, string word)
        {
            if (letters.Length == 0)
            {
                throw new System.Exception();
            }
            if (word == string.Empty)
            {
                return new string[] { };
            }
            // loop through each character of the input word
            for (int i = 0; i < word.Length; i++)
            {
                // get the current character of the word as a string
                string currentChar = word.Substring(i, 1);
                // loop through each element of input array
                for (int j = 0; j < letters.Length; j++)
                {
                    if (letters[j] == currentChar)
                    {
                        // replace the matching char with an empty string
                        letters[j] = string.Empty;
                        break;
                    }
                }
            }
            // how many items in aren't empty strings?
            int count = letters.Count(item => item != string.Empty);
            // instantiate new output array and output count
            string[] output = new string[count];
            int outputCount = 0;
            for (int i = 0; i < letters.Length; i++)
            {
                if (letters[i] != string.Empty)
                {
                    output[outputCount] = letters[i]; // use outputCount so that we 
                    
                outputCount++;
                }
            }
            return output;


            /* MY CODE
            //string[] lettersInWord = word.Split(' ');
            char[] lettersInWord = word.ToCharArray();
            string[] newWordArray = { };
 
            foreach (var letter in lettersInWord)
            {
                Console.WriteLine(letter);
                
                if (letter.Equals(letters))
                {
                    List<char> newWord = new List<char>();
                    newWord.Add(letter);
                    char[] newWordArr = newWord.ToArray();
                    Console.WriteLine(newWordArr);
                }
                
                
            } */
        }
    }
}
