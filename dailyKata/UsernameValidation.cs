﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dailyKata
{
    public class UsernameValidation
    {
        public static void validate()
        {
            try
            {
                int count = 0;
                while (count <= 3)
                {
                    string username = "noroff";
                    int password = 9090;
                    Console.WriteLine("Check username and password: \n" +
                        "Correct user name: noroff \n" +
                        "Correct password: 9090\n" +
                        "-------------------------------");
                    Console.Write("Enter username:");
                    string attemptUsername = Console.ReadLine();
                    Console.Write("Enter password:");
                    int attemptPassword = Convert.ToInt32(Console.ReadLine());

                    if (attemptUsername == username && attemptPassword == password)
                    {
                        Console.WriteLine("Successfully Logged in!");
                        break;
                    }
                    else
                    {
                        count++;
                    }
                    if (count >= 3)
                    {
                        Console.WriteLine("Cannot perform more than 3 attempts. Try again later!");
                        break;
                    }
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Wrong type of input, password must contain numbers");
            }

            /* --- Niek solution ---
            string userName = "noroff";
            int passWord = 9090;
            string givenUsername;
            int givenPassword;
            int loginCount = 0;
            do
            {
                Console.WriteLine("Username: ");
                givenUsername = Console.ReadLine();
                Console.WriteLine("Password: ");
                givenPassword = Convert.ToInt32(Console.ReadLine());
                loginCount++;
            }
            while ((userName != givenUsername || passWord != givenPassword) && loginCount < 3);
            if (loginCount >= 3)
            {
                Console.WriteLine("Cannot perform more than 3 attempts. Try again later!");
            }
            else
            {
                Console.WriteLine("welcome");
            }
             */

            /* --- Saju solution ---
             
            string userName = "noroff";
            int passWord = 9090;
            string givenUsername;
            int givenPassword;
            int loginCount = 0;
            do
            {
                Console.WriteLine("Username: ");
                givenUsername = Console.ReadLine();
                Console.WriteLine("Password: ");
                givenPassword = Convert.ToInt32(Console.ReadLine());
                loginCount++;
            }
            while ((userName != givenUsername || passWord != givenPassword) && loginCount < 3);
            if (loginCount >= 3)
            {
                Console.WriteLine("Cannot perform more than 3 attempts. Try again later!");
            }
            else
            {
                Console.WriteLine("welcome");
            }
             */
        }
    }
}
