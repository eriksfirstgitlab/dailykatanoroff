﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katas
{
    internal class Working9to5
    {
        public static void OverTime(double[] workingDay)
        {
            double startHour = workingDay[0];
            double endHour = workingDay[1];
            int hourlyRate = (int)workingDay[2];
            double overtimeMultiplier = workingDay[3];
            double fullSalary = 0;
            if (isGoodHourFormat(startHour) && isGoodHourFormat(endHour) && isPositive(hourlyRate) && isPositive(overtimeMultiplier))
            {
                if (!isOvertime(endHour))
                {
                    fullSalary = (endHour - startHour) * hourlyRate;
                }
                else
                {
                    fullSalary += (17 - startHour) * hourlyRate;
                    fullSalary += (endHour - 17) * hourlyRate * overtimeMultiplier;
                }
                Console.WriteLine($"Working {startHour} to {endHour}, with hourly rate of {hourlyRate}" +
                                  $" and overtime multiplier of {overtimeMultiplier}, you make: ${fullSalary.ToString("0.00")}");
            }
            else
            {
                Console.WriteLine("The hour format you have given is wrong," +
                    " or either the hourly rate or overtime multiplier is negative!");
            }
        }

        public static bool isOvertime(double endHour)
        {
            return endHour > 17;
        }

        public static bool isGoodHourFormat(double hours)
        {
            return (hours >= 0 && hours <= 24);
        }

        public static bool isPositive(double number)
        {
            return number >= 0;
        }
    }
}
