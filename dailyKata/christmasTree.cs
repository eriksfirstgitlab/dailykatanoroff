﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dailyKata
{
    public class christmasTree
    {
        public static void writeTree()
        {
            Console.WriteLine("          *    ");
            Console.WriteLine("         ***   ");
            Console.WriteLine("        *****  ");
            Console.WriteLine("       ******* ");
            Console.WriteLine("      *********");
        }

        public static void writeAnyTrees(int numberOfTrees)
        {
            for (int i = 0; i < numberOfTrees; i++)
            {
                writeTree();
            }
        }
    }
}
