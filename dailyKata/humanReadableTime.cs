﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace dailyKata
{
    public class humanReadableTime
    {
        public static string GetReadableTime(int seconds)
        {
            //string result = "";
            string output = "";

            if (seconds <= 359999 && seconds >= 0)
            {
                //TimeSpan time = TimeSpan.FromSeconds(seconds);
                //result = time.ToString(@"HH\:MM\:SS");

                TimeSpan span = TimeSpan.FromSeconds(seconds);
                output = string.Format("{0:00}:{1:00}:{2:00}", 
                    Math.Floor(span.TotalHours), 
                    span.Minutes,
                    span.Seconds);
            }
            else
            {
                Console.WriteLine("Wrong input :( cannot exceed 359999 seconds ...");
            }
            return output;
        }
    }
}
