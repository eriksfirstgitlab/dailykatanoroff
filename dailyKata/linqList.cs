﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dailyKata
{
    public class linqList
    {
        public static void linqListOut() 
        {
            int[] ints = {1,2,3,-4,5,-6,7,-8,9,-1,101, 105,10000};

            IEnumerable<int> result =
                from num in ints
                where num > 0
                where num < 100
                orderby num descending
                select num;
 
            foreach (var item in result)
            {
                Console.Write( item + " " );
            }
        }   
    }
}
