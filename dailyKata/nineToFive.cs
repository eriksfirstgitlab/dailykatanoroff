﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dailyKata
{
    public class nineToFive
    {
        public static void OverTime(double start, double end, int rate, double overtimeMultiplier)
        {
            double result = 0;

            if (overtimeMultiplier > 1.5)
            {
                double overtime = (end - start) * rate * overtimeMultiplier;
                result = overtime + rate;
            }
            else
            {
                result = (end - start) * rate;
            }
            result = Math.Round(result,2);
            Console.WriteLine($"${result}");
        }
        public static void OverTime2(double[] input)
        {
            if (input[1] > 17 && input[0] < 17)
            {
                double temp1 = 17 - input[0];
                double temp2 = input[1] - 17;

                double normHour = temp1 * input[2];

                double overTime = temp2 * input[2] * input[3];

                Console.WriteLine("$" + Math.Round(overTime + normHour));
            }
            else if (input[1] <= 17 && input[0] <= 17)
            {
                double temp = input[1] - input[0];
                Console.WriteLine("$" + Math.Round(temp * input[2]));
            }
            else
            {
                Console.WriteLine("You need to work within core hours to get overpayd!");
            }
        }
    }
}
