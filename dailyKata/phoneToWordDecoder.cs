﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dailyKata
{
    public class phoneToWordDecoder
    {
        public static string textToNum(string input)
        {
            string result = "";
            MaskedTextProvider prov = new MaskedTextProvider("CCC-CCC-CCCC");
            MaskedTextProvider prov2 = new MaskedTextProvider("(CCC)CCC-CCCC");
            prov.Set(input);
            prov2.Set(input);

            if (prov.MaskFull || prov2.MaskFull)
            {
                List<char> abc = new List<char>() { 'a', 'b', 'c' };
                List<char> def = new List<char>() { 'd', 'e', 'f' };
                List<char> ghi = new List<char>() { 'g', 'h', 'i' };
                List<char> jkl = new List<char>() { 'j', 'k', 'l' };
                List<char> mno = new List<char>() { 'm', 'n', 'o' };
                List<char> pqrs = new List<char>() { 'p', 'q', 'r', 's' };
                List<char> tuv = new List<char>() { 't', 'u', 'v' };
                List<char> wxyz = new List<char>() { 'w', 'x', 'y', 'z' };
                List<char> resultList = new List<char>();
                input = input.ToLower();
                foreach (char c in input)
                {
                    if (abc.Contains(c)) { resultList.Add('2'); }
                    else if (def.Contains(c)) { resultList.Add('3'); }
                    else if (ghi.Contains(c)) { resultList.Add('4'); }
                    else if (jkl.Contains(c)) { resultList.Add('5'); }
                    else if (mno.Contains(c)) { resultList.Add('6'); }
                    else if (pqrs.Contains(c)) { resultList.Add('7'); }
                    else if (tuv.Contains(c)) { resultList.Add('8'); }
                    else if (wxyz.Contains(c)) { resultList.Add('9'); }
                    else resultList.Add(c);
                }
                result = string.Join("", resultList);
            }
            else
            {
                Console.WriteLine("Wrong input :(");
            }
            return result;
        }
    }
}
