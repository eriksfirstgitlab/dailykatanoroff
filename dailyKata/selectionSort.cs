﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dailyKata
{
    public class selectionSort
    {
        public static void SelectionSort(int[] ints)
        {
            int[] output= new int[ints.Length];
            int lengthOfArray = ints.Length;

            Console.WriteLine("Sorted Array Elements - Step by Step: \n");

            for (int i = 0; i < lengthOfArray; i++)
            {
                output[i] = ints[i];
            }
            int temp, smallest;
            for (int i = 0; i < lengthOfArray; i++)
            {
                smallest = i;
                for (int j = i + 1; j < lengthOfArray; j++)
                {
                    if (output[j] < output[smallest])
                    {
                        smallest = j;
                    }
                    temp = output[smallest];
                    output[smallest] = output[i];
                    output[i] = temp;
                }
                for (int x = 0; x < output.Length; x++)
                {
                    Console.Write($"{output[x]} ");
                }
                Console.WriteLine("\n");
            }
        }
    }
}
