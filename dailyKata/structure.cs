﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dailyKata
{
    public struct Square
    {
        public decimal Length;
        public decimal Breadth;
        public decimal Perimeter;
        public decimal Area;
    }
    public class structure
    {
        public static void Square()
        {
            Square square = new Square();
            Console.WriteLine("Method that returns a structure  :\n" +
                "-------------------------------------------------");
            Console.WriteLine("Input the dimensions of the Square(length and breadth)");
            Console.Write("Length: ");
            square.Length = Convert.ToDecimal(Console.ReadLine());
            Console.Write("Breadth: ");
            square.Breadth = Convert.ToDecimal(Console.ReadLine());

            square.Perimeter = 2 * (square.Length + square.Breadth);
            square.Area = square.Length * square.Breadth;

            Console.WriteLine($"\nPerimeter and Area of the square :\n" +
                $"Length: {square.Length}\n" +
                $"Breadth: {square.Breadth}\n" +
                $"Perimeter: {square.Perimeter}\n" +
                $"Area: {square.Area}");
            Console.ReadLine();
        }
    }
}
